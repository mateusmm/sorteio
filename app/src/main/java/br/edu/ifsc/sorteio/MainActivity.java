package br.edu.ifsc.sorteio;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button btn_sortear = (Button) findViewById(R.id.btn_sorteio);
        final EditText valor1 = (EditText) findViewById(R.id.valor1);
        final EditText valor2 = (EditText) findViewById(R.id.valor2);

        btn_sortear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Integer valor1 = Integer.parseInt(valor1.getText().toString());
                Integer valor2 = Integer.parseInt(valor2.getText().toString());

                double random = (Math.random()*((valor2-valor1)+1))+valor1;

                Toast toast = Toast.makeText(getApplicationContext(), String.valueOf(random), Toast.LENGTH_LONG);
                toast.show();

            }
        });




    }
}